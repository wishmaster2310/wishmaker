module.exports = function(grunt) {

    grunt.initConfig({
        less: {
            compile: {
                files: [{
                    cwd: 'static/less',
                    src: ['style.less'],
                    dest: 'static/css',
                    ext: '.css',
                    expand: true

                }],
                options: {
                    sourceMap: true,
                    sourceMapFilename: 'static/css/style.map',
                    compress: true,
                    sourceMapURL: 'style.map',
                    syncImport: true
                }
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'static/img/uploads/',
                    src: ['*.{png,jpg,gif}'],
                    dest: 'static/img/uploads/'
                }]
            }
        },

        sprite: {
            dist: {
                src: ['static/img/sprites/*.png'],
                destImg: 'static/img/sprite.png',
                destCSS: 'static/less/components/sprite.less',
                cssFormat: 'less',
                algorithm: 'binary-tree',
            }
        },

        watch: {
            options: {
                livereload: true,
                spawn: false,
            },
            configFiles: {
                files: ['Gruntfile.js'],
                options: {
                    reload: true
                }
            },
            less: {
                files: ['static/less/*.less', 'static/less/components/*.less'],
                tasks: ['less'],
            },
            imagemin: {
                files: ['static/img/*.{png,jpg,gif}'],
                tasks: ['newer:imagemin'],
            },
            sprite: {
                files: ['static/img/sprites/*.png'],
                tasks: ['sprite', 'newer:imagemin'],
            },
            html: {
                files: ['*.html'],
                options: {
                    livereload: true
                }
            },
            scripts: {
                files: ['static/js/*.js'],
                options: {
                    livereload: true
                }
            }
        },

        connect: {
            server: {
                options: {
                    livereload: true,
                    port: 3000,
                    hostname: 'localhost',
                    base: '.',
                }
            }
        },
    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', ['connect', 'sprite', 'watch' ]);
};
